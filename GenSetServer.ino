#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#define LED 16
#define KEY 0
const char* hostSsid  = "SSE_GEN_SET";


int LEDState = 0;
int buttonState;
unsigned long timeTick;
ESP8266WebServer WebServer(80);


void timer0_ISR (void){
 // Do some work
 if(buttonState  == LOW){
  LEDState = LEDState ? 0 : 1;
  digitalWrite(LED, LEDState);
  Serial.println(timeTick++);
 }
 
 // Set-up the next interrupt cycle
 timer0_write(ESP.getCycleCount() + 80000000); //80Mhz -> 80*10^6 = 1 second
}


void handleRoot() {
  //digitalWrite ( led, 1 );
  char temp[400];
  int sec = timeTick;
  int min = sec / 60;
  int hr = min / 60;

  snprintf ( temp, 400,

"<html>\
  <head>\
    <meta http-equiv='refresh' content='5'/>\
    <title>GenSet Demo</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>Genset UpTime!</h1>\
    <p>Uptime: %02d:%02d:%02d</p>\
  </body>\
</html>",

    hr, min % 60, sec % 60
  );
  WebServer.send ( 200, "text/html", temp );
  //digitalWrite ( led, 0 );
}





void setup() {
  noInterrupts();

  pinMode(LED, OUTPUT);
  pinMode(KEY, INPUT);
  Serial.begin(115200);

  WiFi.mode(WIFI_AP);
  WiFi.softAP(hostSsid);
  
  WebServer.on("/", handleRoot);
  WebServer.begin();
  Serial.println("HTTP WebServer started");
  
  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
  
  timer0_isr_init();
  timer0_attachInterrupt(timer0_ISR);
  timer0_write(ESP.getCycleCount() + 80000000); //80Mhz -> 80*10^6 = 1 second
  
  interrupts(); 
}

void loop() {
  
  WebServer.handleClient();
  buttonState = digitalRead(KEY);
  delay(1000);
}
